package com.training.doconnect.service;



import java.util.List;

import com.training.doconnect.dto.ResponseDTO;
import com.training.doconnect.entity.Admin;
import com.training.doconnect.entity.Answer;
import com.training.doconnect.entity.Question;
import com.training.doconnect.entity.User;

public interface IAdminService {

	public Admin adminLogin(String email, String password);

	public String adminLogout(Long adminId);

	public Admin adminRegister(Admin admin);

	public List<Question> getUnApprovedQuestions();

	public List<Answer> getUnApprovedAnswers();

	public Question approveQuestion(Long questionId);

	public Answer approveAnswer(Long answerId);

	public ResponseDTO deleteQuestion(Long questionId);

	public ResponseDTO deleteAnswer(Long answerId);

	public User getUser(String email);

	public List<User> getAllUser();

}
