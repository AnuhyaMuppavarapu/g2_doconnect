
CREATE DATABASE  IF NOT EXISTS `doconnectdb` ;
USE `doconnectdb`;





--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` bigint NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone_number` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;

INSERT INTO `admin` VALUES (1,'admin1@gmail.com',_binary '\0','Administrator-1','admin1123','9865433232');

UNLOCK TABLES;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;

CREATE TABLE `answer` (
  `id` bigint NOT NULL,
  `answer` varchar(255) DEFAULT NULL,
  `is_approved` bit(1) DEFAULT NULL,
  `answer_user_id` bigint DEFAULT NULL,
  `question_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKj0qw8d6x09hesrrr7ksxm6t2f` (`answer_user_id`),
  KEY `FK8frr4bcabmmeyyu60qt7iiblo` (`question_id`),
  CONSTRAINT `FK8frr4bcabmmeyyu60qt7iiblo` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`),
  CONSTRAINT `FKj0qw8d6x09hesrrr7ksxm6t2f` FOREIGN KEY (`answer_user_id`) REFERENCES `user` (`id`),
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;

INSERT INTO `answer` VALUES (29,'Angular is a development platform, built on TypeScript. As a platform, Angular includes',_binary '',5,2),(34,'AngularJS: AngularJs is a JavaScript open-source front-end framework that is mainly used to develop single-page web applications(SPAs).',_binary '',33,2),(35,'There are four main types of cloud computing: private clouds, public clouds, hybrid clouds, and multiclouds. ',_binary '',33,32),(36,'Sprint boot is a Java-based spring framework used for Rapid Application Development (to build stand-alone microservices). It has extra support of auto-configuration and embedded application server like tomcat, jetty, etc.',_binary '',33,31),(42,'Spring boot is ahighly used for to develop backend service',_binary '',38,31),(43,'Java is a widely used object-oriented programming language and software platform that runs on billions of devices, including notebook computers, mobile devices, gaming consoles, medical devices and many others.',_binary '',38,3),(44,'Java language was developed in such a way that it does not depend on any hardware or software due to the fact that the compiler compiles the code and then converts it to platform-independent byte code which can be run on multiple systems.',_binary '',38,37);

UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;

INSERT INTO `hibernate_sequence` VALUES (63);

UNLOCK TABLES;

--
-- Table structure for table `image_model`
--

DROP TABLE IF EXISTS `image_model`;

CREATE TABLE `image_model` (
  `id` bigint NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pic_byte` blob,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Dumping data for table `image_model`
--

LOCK TABLES `image_model` WRITE;

UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;

CREATE TABLE `question` (
  `id` bigint NOT NULL,
  `is_approved` bit(1) DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `topic` varchar(255) DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4ekrlbqiybwk8abhgclfjwnmc` (`user_id`),
  CONSTRAINT `FK4ekrlbqiybwk8abhgclfjwnmc` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;

INSERT INTO `question` VALUES (2,_binary '','what is Angular?','Angular',1),(3,_binary '','what is java?','Java',1),(30,_binary '','What are the Spring Boot key components?','Springboot',5),(31,_binary '','what is spring boot?','Springboot',5),(32,_binary '','type of cloud services are there?','Cloud',5),(37,_binary '','Why is Java a platform independent language?','Java',33),(39,_binary '','wipro company ceo?','Others',38),(40,_binary '','wipro Headquarters located in?','Others',38),(41,_binary '','Can we change port number in springboot application?','Springboot',38),(45,_binary '','who is our prime minister?','Others',1),(49,_binary '','What are the two types of Exceptions in Java? Which are the differences between them?','Java',1),(50,_binary '',' What is the difference between an Applet and a Java Application?','Java',1),(51,_binary '','What is JVM? Why is Java called the \"Platform Independent Programming Language”?','Java',1),(52,_binary '','What is a Java Applet?','Java',1),(53,_binary '','What are the Data Types supported by Java? What are Autoboxing and Unboxing?','Java',1),(54,_binary '',' How is Java different from C++?','Java',1),(55,_binary '','Pointers are used in C/ C++. Why does Java not make use of pointers?','Java',1),(56,_binary '','java interfaces?','Java',1),(57,_binary '','Is Java is OOPS Language?','Java',1),(58,_binary '','Classes in Java?','Java',1),(59,_binary '','java collections?','Java',1),(60,_binary '','Abstract classes in java?','Java',1),(61,_binary '','How to Instantiate Objects in java','Java',1);

UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` bigint NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone_number` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;

INSERT INTO `user` VALUES (1,'durgaprasad@gmail.com',_binary '','Durgaprasad Meesala','durgaprasad123','8376176964'),(5,'sneka@gmail.com',_binary '\0','sneka sri','sneka123','9834742832'),(33,'nirbhay@gmail.com',_binary '\0','Nirbhay Garg','durgaprasad123','9876554343'),(38,'anuhya@gmail.com',_binary '\0','Anuhya  Muppavarapu','anuhya123','9887656565'),(62,'abhinav@gmail.com',_binary '','Abhinav  chauhan','abhinav123','9876578985');

UNLOCK TABLES;




CREATE DATABASE  IF NOT EXISTS `doconnectchatdb` ;
USE `doconnectchatdb`;









--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;

INSERT INTO `hibernate_sequence` VALUES (5);

UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;


CREATE TABLE `message` (
  `id` bigint NOT NULL,
  `from_user` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;

INSERT INTO `message` VALUES (1,'Durgaprasad Meesala','hi'),(2,'Durgaprasad Meesala','hi'),(3,'sneka sri','hello'),(4,'Anuhya  Muppavarapu','hi');

UNLOCK TABLES;






